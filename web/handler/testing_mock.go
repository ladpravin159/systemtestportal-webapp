/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/group"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/system"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/task"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/store/dummydata"
)

// CaseAdderMock is a mock of the CaseAdder
type CaseAdderMock struct {
	CallerMock
	*test.Case
	Err error
}

// Add mocks adding a case to the store
func (m *CaseAdderMock) Add(testCase *test.Case) error {
	m.Called++
	m.Case = testCase
	return m.Err
}

// ProjectAdderMock mocks the ProjectAdder
type ProjectAdderMock struct {
	CallerMock
	*project.Project
	Err error
}

// Add mocks adding a project to the store
func (p *ProjectAdderMock) Add(pr *project.Project) error {
	p.Called++
	p.Project = pr
	return p.Err
}

// ProjectExistenceMock mocks the ProjectExistenceChecker
type ProjectExistenceMock struct {
	CallerMock
	id.ProjectID
	Exst bool
}

// Exists mocks checking for the existence of a project
func (m *ProjectExistenceMock) Exists(id id.ProjectID) (bool, error) {
	m.Called++
	m.ProjectID = id
	return m.Exst, nil
}

// TestExistenceCheckerMock mocks the TestExistenceChecker
type TestExistenceCheckerMock struct {
	CallerMock
	CaseID id.TestID
}

// Exists mocks checking for the existence of a test
func (m *TestExistenceCheckerMock) Exists(id id.TestID) (bool, error) {
	m.Called++
	m.CaseID = id
	return false, nil
}

// CaseDeleterMock mocks the CaseDeleter
type CaseDeleterMock struct {
	CallerMock
	id.TestID
}

// Delete mocks deleting a case from the store
func (m *CaseDeleterMock) Delete(testCase *test.Case) error {
	m.Called++
	m.TestID = testCase.ID()
	return nil
}

// CaseUpdaterMock mocks the CaseUpdater
type CaseUpdaterMock struct {
	CaseDeleterMock
	CaseAdderMock
	CaseRenamerMock
}

// CaseProtocolRenamerMock mocks the CaseProtocolRenamer
type CaseProtocolRenamerMock struct {
	CallerMock
	Old id.TestID
	New id.TestID
}

// HandleCaseRename mocks renaming a test
func (m *CaseProtocolRenamerMock) HandleCaseRename(old, new id.TestID) error {
	m.Called++
	m.Old = old
	m.New = new
	return nil
}

// CaseRenamerMock mocks the CaseRenamer
type CaseRenamerMock struct {
	CallerMock
	Old id.TestID
	New id.TestID
}

// Rename mocks the renaming of a test
func (m *CaseRenamerMock) Rename(old, new id.TestID) error {
	m.CallerMock.Called++
	m.Old = old
	m.New = new
	return nil
}

// CaseGetterMock mocks the CaseGetter
type CaseGetterMock struct {
	CallerMock
	CaseID id.TestID
}

// Get mocks the retrieval of a case from a test id
func (m *CaseGetterMock) Get(caseID id.TestID) (*test.Case, bool, error) {
	m.Called++
	m.CaseID = caseID
	return DummyTestCase, true, nil
}

// UserListerMock mocks the UserLister
type UserListerMock struct {
	CallerMock
	Users []*user.User
	Err   error
}

// List mocks listing users
func (m *UserListerMock) List() ([]*user.User, error) {
	m.Called++
	return m.Users, m.Err
}

//Adds Users to the UserLister
func (m *UserListerMock) Initialize(users ...*user.User) (error) {
	m.Users = users
	m.Called++
	return m.Err
}

type UserAdderMock struct {
	CallerMock
	Err error
}

func (m *UserAdderMock) Add(pur *user.PasswordUser) (error) {
	m.Called++
	return m.Err
}

type UserValidatorMock struct {
	CallerMock
	User   user.User
	Exists bool
	Err    error
}

func (m *UserValidatorMock) Validate(identifier, password string) (*user.User, bool, error) {
	m.Called++
	return DummyUser, true, m.Err
}

// GroupAdderMock mocks the GroupAdder
type GroupAdderMock struct {
	CallerMock
	group *group.Group
	Err   error
}

// Add mocks adding a group to the store
func (m *GroupAdderMock) Add(group *group.Group) error {
	m.Called++
	m.group = group
	return m.Err
}

// ActorExistenceCheckerMock mocks the ActorExistenceChecker
type ActorExistenceCheckerMock struct {
	CallerMock
	id   id.ActorID
	Exst bool
	err  error
}

// Exists mocks checking for the existence of an actor
func (m *ActorExistenceCheckerMock) Exists(id id.ActorID) (bool, error) {
	m.Called++
	m.id = id
	return m.Exst, m.err
}

// CaseListerMock mocks the CaseLister
type CaseListerMock struct {
	CallerMock
	Project  id.ProjectID
	CaseList []*test.Case
	Err      error
}

// List mocks listing cases
func (m *CaseListerMock) List(projectID id.ProjectID) ([]*test.Case, error) {
	m.Called++
	m.Project = projectID
	return m.CaseList, m.Err
}

// SequenceListerMock mocks the SequenceLister
type SequenceListerMock struct {
	CallerMock
	Project id.ProjectID
	Err     error
}

// List mocks listing sequences
func (m *SequenceListerMock) List(projectID id.ProjectID) ([]*test.Sequence, error) {
	m.Called++
	m.Project = projectID
	return []*test.Sequence{}, m.Err

}

// CaseProtocolListerMock mocks the CaseProtocolsLister
type CaseProtocolListerMock struct {
	CallerMock
	testCaseID id.TestID
	protocolID id.ProtocolID
	Protocols  []test.CaseExecutionProtocol
	Protocol   test.CaseExecutionProtocol
	Err        error
}

// GetCaseExecutionProtocols mocks retrieving protocols of a case
func (m *CaseProtocolListerMock) GetCaseExecutionProtocols(testCaseID id.TestID) ([]test.CaseExecutionProtocol, error) {
	m.Called++
	m.testCaseID = testCaseID
	return m.Protocols, m.Err
}

// GetCaseExecutionProtocols mocks retrieving protocols of a case for a project
func (m *CaseProtocolListerMock) GetCaseExecutionProtocolsForProject(projectID id.ProjectID) ([]test.CaseExecutionProtocol, error) {
	m.Called++
	m.Protocols = []test.CaseExecutionProtocol{*DummyTestCaseExecutionProtocol}
	return m.Protocols, m.Err
}

// GetCaseExecutionProtocol mocks retrieving protocol of a protocolID
func (m *CaseProtocolListerMock) GetCaseExecutionProtocol(protocolID id.ProtocolID) (test.CaseExecutionProtocol, error) {
	m.Called++
	m.protocolID = protocolID
	return m.Protocol, m.Err
}

// ProjectDeleterMock mocks the ProjectDeleter
type ProjectDeleterMock struct {
	CallerMock
	id.ProjectID
	Err error
}

// Delete mocks deleting a project from the store
func (m *ProjectDeleterMock) Delete(projectID id.ProjectID) error {
	m.Called++
	m.ProjectID = projectID
	return m.Err
}

// SequenceDeletionMock mocks the SequenceDeleter
type SequenceDeletionMock struct {
	CallerMock
	ID id.TestID
}

// Delete mocks deleting a sequence from the store
func (m *SequenceDeletionMock) Delete(testSequence *test.Sequence) error {
	m.Called++
	m.ID = testSequence.ID()
	return nil
}

// SequenceAdderMock mocks the SequenceAdder
type SequenceAdderMock struct {
	CallerMock
	TestSequence *test.Sequence
	Err          error
}

// Add mocks adding a sequence to the store
func (m *SequenceAdderMock) Add(testSequence *test.Sequence) error {
	m.Called++
	m.TestSequence = testSequence
	return m.Err
}

// SequenceRenamerMock mocks the SequenceRenamer
type SequenceRenamerMock struct {
	CallerMock
	old, new id.TestID
}

// Rename mocks renaming a sequence
func (m *SequenceRenamerMock) Rename(old, new id.TestID) error {
	m.Called++
	m.old = old
	m.new = new
	return nil
}

// SequenceUpdaterMock mocks the SequenceUpdater
type SequenceUpdaterMock struct {
	SequenceAdderMock
	SequenceRenamerMock
	SequenceDeletionMock
}

// SequenceRenameHandlerMock mocks the SequenceRenameHandler
type SequenceRenameHandlerMock struct {
	CallerMock
	old, new id.TestID
}

// HandleSequenceRename mocks renaming a sequence
func (srh *SequenceRenameHandlerMock) HandleSequenceRename(old, new id.TestID) error {
	srh.Called++
	srh.old = old
	srh.new = new
	return nil
}

// SequenceProtocolListerMock mocks the SequenceProtocolLister
type SequenceProtocolListerMock struct {
	CallerMock
	sequenceID id.TestID
	protocolID id.ProtocolID
	Protocols  []test.SequenceExecutionProtocol
	Protocol   test.SequenceExecutionProtocol
	Err        error
}

// GetSequenceExecutionProtocols mocks retrieving protocols of a sequence
func (m *SequenceProtocolListerMock) GetSequenceExecutionProtocols(sequenceID id.TestID) (
	[]test.SequenceExecutionProtocol, error,
) {
	m.Called++
	m.sequenceID = sequenceID
	m.Protocols = []test.SequenceExecutionProtocol{*DummyTestSequenceExecutionProtocol}
	return m.Protocols, m.Err
}

// GetSequenceExecutionProtocols mocks retrieving protocols of a sequence for project
func (m *SequenceProtocolListerMock) GetSequenceExecutionProtocolsForProject(projectID id.ProjectID) ([]test.SequenceExecutionProtocol, error) {
	m.Called++
	m.Protocols = []test.SequenceExecutionProtocol{*DummyTestSequenceExecutionProtocol}
	return m.Protocols, m.Err
}

// GetSequenceExecutionProtocol mocks retrieving a protocol of a sequence
func (m *SequenceProtocolListerMock) GetSequenceExecutionProtocol(protocolID id.ProtocolID) (test.SequenceExecutionProtocol, error) {
	m.Called++
	m.protocolID = protocolID
	return m.Protocol, m.Err
}

// ProjectListerMock mocks the ProjectLister
type ProjectListerMock struct {
	CallerMock
	Projects []*project.Project
	Err      error
}

// ListAll mocks listing projects
func (m *ProjectListerMock) ListAll() ([]*project.Project, error) {
	m.Called++
	return m.Projects, m.Err
}

// ListForOwner mocks listing projects for an owner
func (m *ProjectListerMock) ListForOwner(owner id.ActorID) ([]*project.Project, error) {
	m.Called++
	return m.Projects, m.Err
}

// ListForMember mocks listing projects where the actor is a member
func (m *ProjectListerMock) ListForMember(owner id.ActorID) ([]*project.Project, error) {
	m.Called++
	return m.Projects, m.Err
}

// ListForActor mocks listing all projects that the actor has access to
func (m *ProjectListerMock) ListForActor(actor id.ActorID) ([]*project.Project, error) {
	m.Called++
	return m.Projects, m.Err
}

// List mocks listing projects
func (m *ProjectListerMock) ListPublic() ([]*project.Project, error) {
	m.Called++
	return m.Projects, m.Err
}

// ListInternal mocks listing internal projects
func (m *ProjectListerMock) ListInternal() ([]*project.Project, error) {
	m.Called++
	return m.Projects, m.Err
}

// ListPrivate mocks listing private projects
func (m *ProjectListerMock) ListPrivate(actor id.ActorID) ([]*project.Project, error) {
	m.Called++
	return m.Projects, m.Err
}

// GroupListerMock mocks the GroupLister
type GroupListerMock struct {
	CallerMock
	Groups []*group.Group
	Err    error
}

// List mocks listing groups
func (m *GroupListerMock) List() ([]*group.Group, error) {
	m.Called++
	return m.Groups, m.Err
}

// UserRetrieverMock mocks the UserRetriever interface
type UserRetrieverMock struct {
	CallerMock
	User      *user.User
	UserFound bool
	Err       error
}

// Get mocks getting a user
func (userRetrieverMock *UserRetrieverMock) Get(userID id.ActorID) (*user.User, bool, error) {
	userRetrieverMock.Called++
	return userRetrieverMock.User, userRetrieverMock.UserFound, userRetrieverMock.Err
}

// AuthMock mocks the Auth interface
type AuthMock struct {
	GivenIdentifier string
	GivenPassword   string
	accept          bool
	returnUser      *user.User
	returnError     error
}

// Validate is a mock method
func (a *AuthMock) Validate(identifier string, password string) (*user.User, bool, error) {
	a.GivenIdentifier = identifier
	a.GivenPassword = password
	return a.returnUser, a.accept, a.returnError
}

// NewAuthMock creates a new Auth mock
func NewAuthMock(returnedUser *user.User, returnedError error, accept bool) *AuthMock {
	return &AuthMock{
		"",
		"",
		accept,
		returnedUser,
		returnedError,
	}
}

// RegisterServerMock mocks the RegisterServer
type RegisterServerMock struct {
	GetUserIDIn     id.ActorID
	GetUserMailIn   string
	GetUserUserOut  *user.User
	GetUserFoundOut bool
	AddUserUserIn   *user.PasswordUser
	AddUserErrorOut error
}

// Get mocks the retrieval of the user from a given id
func (usm *RegisterServerMock) Get(id id.ActorID) (*user.User, bool, error) {
	usm.GetUserIDIn = id
	return usm.GetUserUserOut, usm.GetUserFoundOut, nil
}

// GetByMail mocks the retrieval of the user from a given mail
func (usm *RegisterServerMock) GetByMail(mail string) (*user.User, bool, error) {
	usm.GetUserMailIn = mail
	return usm.GetUserUserOut, usm.GetUserFoundOut, nil
}

// Add mocks adding a user to the store
func (usm *RegisterServerMock) Add(u *user.PasswordUser) error {
	usm.AddUserUserIn = u
	return usm.AddUserErrorOut
}

/*
	Execution mocks
*/

// CaseSessionMock mocks the CaseSession interface
type CaseSessionMock struct {
	CallerMock
	CaseSessionUpdaterMock
	TimeSessionMock
	Err error
}

// RemoveCurrentCaseProtocol mocks removing the current case protocol from the session
func (csm *CaseSessionMock) RemoveCurrentCaseProtocol(w http.ResponseWriter, r *http.Request) error {
	csm.Called++
	return csm.Err
}

// SequenceSessionMock mocks the SequenceSession interface
type SequenceSessionMock struct {
	CallerMock
	SequenceSessionGetterMock
	SequenceSessionSetterMock
	TimeSessionMock
	Err error
}

// RemoveCurrentSequenceProtocol mocks removing the current sequence protocol from the session
func (sequenceSessionMock *SequenceSessionMock) RemoveCurrentSequenceProtocol(w http.ResponseWriter,
	r *http.Request) error {
	sequenceSessionMock.Called++
	return sequenceSessionMock.Err
}

// TimeSessionMock mocks the TimeSession interface
type TimeSessionMock struct {
	CallerMock
	Duration *duration.Duration
	Err      error
}

// GetDuration mocks getting the duration from the session
func (tsm *TimeSessionMock) GetDuration(r *http.Request) (*duration.Duration, error) {
	tsm.Called++
	return tsm.Duration, tsm.Err
}

// SetDuration mocks setting the duration
func (tsm *TimeSessionMock) SetDuration(w http.ResponseWriter, r *http.Request, duration *duration.Duration) error {
	tsm.Called++
	return tsm.Err
}

// CaseSessionUpdaterMock mocks the CaseSessionUpdater interface
type CaseSessionUpdaterMock struct {
	CallerMock
	CaseSessionGetterMock
	CaseSessionSetterMock
}

// CaseSessionGetterMock mocks the CaseSessionGetter interface
type CaseSessionGetterMock struct {
	CallerMock
	CaseExecutionProtocol *test.CaseExecutionProtocol
	Err                   error
}

// GetCurrentCaseProtocol mocks getting the current case protocol
func (csgm *CaseSessionGetterMock) GetCurrentCaseProtocol(r *http.Request) (*test.CaseExecutionProtocol, error) {
	csgm.Called++
	return csgm.CaseExecutionProtocol, csgm.Err
}

// CaseSessionSetterMock mocks the CaseSessionSetter interface
type CaseSessionSetterMock struct {
	CallerMock
	Err error
}

// SetCurrentCaseProtocol mocks setting the current case protocol
func (cssm *CaseSessionSetterMock) SetCurrentCaseProtocol(w http.ResponseWriter, r *http.Request,
	protocol *test.CaseExecutionProtocol) error {
	cssm.Called++
	return cssm.Err
}

// SequenceSessionGetterMock mocks the SequenceSessionGetter interface
type SequenceSessionGetterMock struct {
	CallerMock
	SeqExecProtocol *test.SequenceExecutionProtocol
}

// GetCurrentSequenceProtocol mocks getting the current sequence protocol
func (ssgm *SequenceSessionGetterMock) GetCurrentSequenceProtocol(r *http.Request) (*test.SequenceExecutionProtocol,
	error) {
	ssgm.Called++
	return ssgm.SeqExecProtocol, nil
}

// SequenceSessionSetterMock mocks the SequenceSessionSetter interface
type SequenceSessionSetterMock struct {
	CallerMock
	Err error
}

// SetCurrentSequenceProtocol mocks setting the current sequence protocol
func (sequenceSessionSetterMock *SequenceSessionSetterMock) SetCurrentSequenceProtocol(w http.ResponseWriter,
	r *http.Request, protocol *test.SequenceExecutionProtocol) error {
	sequenceSessionSetterMock.Called++
	return sequenceSessionSetterMock.Err
}

// ProtocolListerMock mocks the ProtocolLister interface
type ProtocolListerMock struct {
	CallerMock
	TestVersionID id.TestVersionID
	ProtocolIDs   []id.ProtocolID
	protocolID    id.ProtocolID
	testCaseID    id.TestID
	Protocols     []test.CaseExecutionProtocol
	Protocol      test.CaseExecutionProtocol
	Err           error
}

// GetCaseExecutionProtocols mocks retrieving protocols of a case
func (m *ProtocolListerMock) GetCaseExecutionProtocols(testCaseID id.TestID) ([]test.CaseExecutionProtocol, error) {
	m.Called++
	m.testCaseID = testCaseID
	m.Protocols = []test.CaseExecutionProtocol{*DummyTestCaseExecutionProtocol}
	return m.Protocols, m.Err
}

// GetCaseExecutionProtocol mocks retrieving protocol of a protocolID
func (m *ProtocolListerMock) GetCaseExecutionProtocol(protocolID id.ProtocolID) (test.CaseExecutionProtocol, error) {
	m.Called++
	m.protocolID = protocolID
	m.Protocol = *DummyTestCaseExecutionProtocol
	return m.Protocol, m.Err
}

// GetTestVersionProtocols mocks getting the protocols of a test version
func (protocolListerMock *ProtocolListerMock) GetTestVersionProtocols(testVersionID id.TestVersionID) ([]id.ProtocolID,
	error) {
	protocolListerMock.Called++
	protocolListerMock.TestVersionID = testVersionID
	return protocolListerMock.ProtocolIDs, protocolListerMock.Err
}

// CaseProtocolAdderMock mocks the CaseProtocolAdder interface
type CaseProtocolAdderMock struct {
	CallerMock
	CaseExecutionProtocol test.CaseExecutionProtocol
	Err                   error
	CaseVersion           test.CaseVersion
}

// AddCaseProtocol mocks adding a case protocol to the store
func (caseProtocolAdderMock *CaseProtocolAdderMock) AddCaseProtocol(r *test.CaseExecutionProtocol, caseVersion test.CaseVersion) (err error) {
	caseProtocolAdderMock.Called++
	caseProtocolAdderMock.CaseExecutionProtocol = *r
	caseProtocolAdderMock.CaseVersion = caseVersion
	return caseProtocolAdderMock.Err
}

// CaseProtocolGetterMock mocks the CaseProtocolGetter interface
type CaseProtocolGetterMock struct {
	CallerMock
	CaseExecutionProtocol test.CaseExecutionProtocol
	ProtocolID            id.ProtocolID
	Err                   error
}

// GetCaseExecutionProtocol mocks getting a case protocol from the session
func (caseProtocolGetterMock *CaseProtocolGetterMock) GetCaseExecutionProtocol(protocolID id.ProtocolID) (
	test.CaseExecutionProtocol, error) {
	caseProtocolGetterMock.Called++
	caseProtocolGetterMock.ProtocolID = protocolID
	return caseProtocolGetterMock.CaseExecutionProtocol, caseProtocolGetterMock.Err
}

// CaseProtocolStoreMock mocks the CaseProtocolStore interface
type CaseProtocolStoreMock struct {
	CallerMock
	TestCaseID             id.TestID
	CaseExecutionProtocols []test.CaseExecutionProtocol
	CaseProtocolAdderMock
	CaseProtocolGetterMock
	Err                    error
}

// GetCaseExecutionProtocols mocks getting all protocol of a case
func (caseProtocolStoreMock *CaseProtocolStoreMock) GetCaseExecutionProtocols(testCaseID id.TestID) (
	[]test.CaseExecutionProtocol, error) {
	caseProtocolStoreMock.Called++
	caseProtocolStoreMock.TestCaseID = testCaseID
	return caseProtocolStoreMock.CaseExecutionProtocols, caseProtocolStoreMock.Err
}

func (caseProtocolStoreMock *CaseProtocolStoreMock) GetCaseExecutionProtocolsForProject(projectID id.ProjectID) (
	[]test.CaseExecutionProtocol, error) {

	caseProtocolStoreMock.Called++
	return caseProtocolStoreMock.CaseExecutionProtocols, caseProtocolStoreMock.Err
}

// SequenceProtocolAdderMock mocks the SequenceProtocolAdder interface
type SequenceProtocolAdderMock struct {
	CallerMock
	SequenceExecutionProtocol *test.SequenceExecutionProtocol
	SequenceVersion           test.SequenceVersion
	Err                       error
}

// AddSequenceProtocol mocks adding a sequence protocol to the store
func (sequenceProtocolAdderMock *SequenceProtocolAdderMock) AddSequenceProtocol(
	r *test.SequenceExecutionProtocol, sequenceVersion test.SequenceVersion) error {
	sequenceProtocolAdderMock.Called++
	sequenceProtocolAdderMock.SequenceExecutionProtocol = r
	sequenceProtocolAdderMock.SequenceVersion = sequenceVersion
	return sequenceProtocolAdderMock.Err
}

// TaskGetterMock mocks the TaskGetter
type TaskGetterMock struct {
	CallerMock
	TaskList *task.List
	Tasks    []*task.Item
}

// GetTasksForTest mocks getting tasks for the given tests
func (mock TaskGetterMock) GetTasksForTest(testID id.TestID) ([]*task.Item, error) {
	mock.Called++
	return mock.Tasks, nil
}

// TaskListGetterMock mocks the TaskListGetter interface
type TaskListGetterMock struct {
	CallerMock
	TaskList *task.List
	Username string
	Err      error
}

// Get mocks getting the to do-list for a user with the given username
func (taskListGetterMock TaskListGetterMock) Get(username string) (*task.List, error) {
	taskListGetterMock.Called++
	taskListGetterMock.Username = username
	return task.NewList(username), taskListGetterMock.Err
}

// TaskListAdderMock mocks the TaskListAdder interface
type TaskListAdderMock struct {
	CallerMock
	TaskList task.List
	Err      error
}

// Add mocks adding a taskList to the store
func (taskListAdderMock TaskListAdderMock) Add(taskList task.List) error {
	taskListAdderMock.Called++
	taskListAdderMock.TaskList = taskList
	return taskListAdderMock.Err
}

type TestSequenceStoreMock struct {
	Sequence test.Sequence
	Err      error
}

// Mocked method that always returns valid results
func (testSequenceStoreMock *TestSequenceStoreMock) Get(sequenceID id.TestID) (*test.Sequence, bool, error) {
	return &dummydata.Sequences[0], true, nil
}

type CommentStoreMock struct {
	CommentAdderMock
	CommentGetterMock
	CommentDeleterMock
	Err error
}

type CommentAdderMock struct {
	called int
}

func (adderMock CommentAdderMock) InsertComment(comment *comment.Comment, testObject interface{}, user *user.User) error {
	adderMock.called++
	return nil
}

func (adderMock CommentAdderMock) UpdateComment(*comment.Comment) error {
	adderMock.called++
	return nil
}

func (adderMock CommentAdderMock) Calls() int {
	return adderMock.called
}

type CommentGetterMock struct {
	called int
}

func (getterMock CommentGetterMock) GetCommentsForTest(testObject interface{}, project *project.Project, requester *user.User) ([]*comment.Comment, error) {
	getterMock.called++
	return nil, nil
}

func (getterMock CommentGetterMock) GetComment(commentId int64, project *project.Project) (*comment.Comment, error) {
	getterMock.called++
	return nil, nil
}

func (getterMock CommentGetterMock) Calls() int {
	return getterMock.called
}

type CommentDeleterMock struct {
	called int
}

func (deleterMock CommentDeleterMock) DeleteCommentsForTest(testObject interface{}) error {
	deleterMock.called++
	return nil
}

func (deleterMock CommentDeleterMock) Calls() int {
	return deleterMock.called
}

type SystemSettingsStoreMock struct {
	SystemSettingsGetMock
	SystemSettingsAddMock
}

type SystemSettingsGetMock struct {
	CallerMock
}

func (systemSettingsGetMock SystemSettingsGetMock) GetSystemSettings() (*system.SystemSettings, error) {
	systemSettingsGetMock.Called++
	return &system.SystemSettings{Id: 1}, nil
}

func (systemSettingsGetMock SystemSettingsGetMock) Calls() int {
	return systemSettingsGetMock.Called
}

type SystemSettingsAddMock struct {
	CallerMock
}

func (systemSettingsAddMock SystemSettingsAddMock) UpdateSystemSettings(systemSettings *system.SystemSettings) error {
	systemSettingsAddMock.Called++
	return nil
}

func (systemSettingsAddMock SystemSettingsAddMock) InsertSystemSettings(systemSettings *system.SystemSettings) error {
	systemSettingsAddMock.Called++
	return nil
}

func (systemSettingsAddMock SystemSettingsAddMock) Calls() int {
	return systemSettingsAddMock.Called
}

type ProjectLabelUpdaterMock struct {
	ProjectAdderMock
	ProjectLabelRenamerMock
}

type ProjectLabelRenamerMock struct {
	CallerMock
	*project.Project
	old project.Label
	new project.Label
	Err error
}

func (p *ProjectLabelRenamerMock) RenameProjectLabel(pr *project.Project, old project.Label, new project.Label) error {
	p.Called++
	p.Project = pr
	var l []project.Label
	l = append(l, old, new)
	p.Labels = l
	return p.Err

}
