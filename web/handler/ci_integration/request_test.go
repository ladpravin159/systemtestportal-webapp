/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package ci_integration

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestIntegrationPost(t *testing.T) {

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:   handler.DummyProject,
			middleware.ContainerKey: handler.DummyUser,
		},
	)

	invalidBody := "{invalid;;;]"
	invalidTokenBody := "{\"version\":\"testVersion\", \"token\":\"invalid\"}"
	validBody := "{\"version\":\"testVersion\", \"variant\":\"testVariant\", \"token\":\"token\"}"

	handler.Suite(t,
		handler.CreateTest("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseLister := &handler.CaseListerMock{}
				caseProtocolLister := &handler.CaseProtocolListerMock{}
				taskLister := &handler.TaskListGetterMock{}
				taskAdder := &handler.TaskListAdderMock{}
				projectAdder := &handler.ProjectAdderMock{}
				return IntegrationPost(caseLister, caseProtocolLister, taskLister, taskAdder, projectAdder), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(caseLister, 0),
					handler.HasCalls(caseProtocolLister, 0),
				)
			},
			handler.NewRequest(handler.EmptyCtx, http.MethodPost, handler.NoParams, ""),
		),
		handler.CreateTest("Invalid json body",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseLister := &handler.CaseListerMock{}
				caseProtocolLister := &handler.CaseProtocolListerMock{}
				taskLister := &handler.TaskListGetterMock{}
				taskAdder := &handler.TaskListAdderMock{}
				projectAdder := &handler.ProjectAdderMock{}
				return IntegrationPost(caseLister, caseProtocolLister, taskLister, taskAdder, projectAdder), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(caseLister, 0),
					handler.HasCalls(caseProtocolLister, 0),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, invalidBody),
		),
		handler.CreateTest("Invalid token in body",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseLister := &handler.CaseListerMock{}
				caseProtocolLister := &handler.CaseProtocolListerMock{}
				taskLister := &handler.TaskListGetterMock{}
				taskAdder := &handler.TaskListAdderMock{}
				projectAdder := &handler.ProjectAdderMock{}
				return IntegrationPost(caseLister, caseProtocolLister, taskLister, taskAdder, projectAdder), handler.Matches(
					handler.HasStatus(http.StatusForbidden),
					handler.HasCalls(caseLister, 0),
					handler.HasCalls(caseProtocolLister, 0),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, invalidTokenBody),
		),
		handler.CreateTest("Not all tests executed",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseLister := &handler.CaseListerMock{}
				caseProtocolLister := &handler.CaseProtocolListerMock{}
				taskLister := &handler.TaskListGetterMock{}
				taskAdder := &handler.TaskListAdderMock{}
				projectAdder := &handler.ProjectAdderMock{}
				return IntegrationPost(caseLister, caseProtocolLister, taskLister, taskAdder, projectAdder), handler.Matches(
					handler.HasStatus(http.StatusForbidden),
					handler.HasCalls(caseLister, 0),
					handler.HasCalls(caseProtocolLister, 0),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, invalidTokenBody),
		),
		handler.CreateTest("All tests executed successfully",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseLister := &handler.CaseListerMock{
					CaseList: []*test.Case{
						{
							TestCaseVersions: []test.CaseVersion{
								{
									Versions: map[string]*project.Version{
										"testVersion": {
											Name:     "testVersion",
											Variants: []project.Variant{{"testVariant"}},
										},
									},
									Description: "",
								},
							},
						},
					},
				}
				caseProtocolLister := &handler.CaseProtocolListerMock{
					Protocols: []test.CaseExecutionProtocol{
						{
							SUTVersion:  "testVersion",
							SUTVariant:  "testVariant",
							Result:      test.Result(1),
							TestVersion: id.NewTestVersionID(id.NewTestID(id.NewProjectID("", ""), "", true), 1),
						},
					},
				}
				taskLister := &handler.TaskListGetterMock{}
				taskAdder := &handler.TaskListAdderMock{}
				projectAdder := &handler.ProjectAdderMock{}
				return IntegrationPost(caseLister, caseProtocolLister, taskLister, taskAdder, projectAdder), handler.Matches(
					handler.HasStatus(http.StatusOK),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, validBody),
		),
		handler.CreateTest("Not all tests executed successfully",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseLister := &handler.CaseListerMock{
					CaseList: []*test.Case{
						{TestCaseVersions: []test.CaseVersion{{Versions: map[string]*project.Version{
							"testVersion": {
								Name:     "testVersion",
								Variants: []project.Variant{{"testVariant"}},
							},
						}}},
						},
					},
				}
				caseProtocolLister := &handler.CaseProtocolListerMock{
					Protocols: []test.CaseExecutionProtocol{
						{
							SUTVersion:  "testVersion",
							SUTVariant:  "testVariant",
							Result:      test.Result(0),
							TestVersion: id.NewTestVersionID(id.NewTestID(id.NewProjectID("", ""), "", true), 1),
						},
					},
				}
				taskLister := &handler.TaskListGetterMock{}
				taskAdder := &handler.TaskListAdderMock{}
				projectAdder := &handler.ProjectAdderMock{}
				return IntegrationPost(caseLister, caseProtocolLister, taskLister, taskAdder, projectAdder), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, validBody),
		),
	)

}
