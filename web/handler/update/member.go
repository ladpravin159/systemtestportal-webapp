/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"encoding/json"
	"net/http"

	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

const (
	failedMembership      = "failed membership"
	unableToUpdateRole    = "We were unable to update the role of this user."
	changeRoleOwner       = "change role of owner"
	cantChangeRoleOfOwner = "You are not allowed to change the role of the owner"
)

func MemberUpdate(us middleware.UserRetriever, ps handler.ProjectAdder) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.User == nil || c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).EditPermissions {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		mID := r.FormValue(httputil.Member)
		var member string
		err := json.Unmarshal([]byte(mID), &member)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		rID := r.FormValue(httputil.Role)
		var role string
		err = json.Unmarshal([]byte(rID), &role)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tid := id.ActorID(member)
		//Check if user really exists
		m, ok, err := us.Get(tid)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		if !ok {
			errors.ConstructStd(http.StatusInternalServerError,
				failedMembership, unableToUpdateRole, r).
				WithLogf("Unable to get the user with id %v!", tid.Actor()).
				WithStackTrace(1).
				WithRequestDump(r).
				Respond(w)
			return
		}
		//check if requested user is owner, give error if it is
		if strings.Compare(c.Project.Owner.Actor(), m.Name) == 0 {

			er := errors.ConstructStd(http.StatusBadRequest,
				changeRoleOwner, cantChangeRoleOfOwner, nil).
				WithLogf("Tried to change role of owner.").
				WithStackTrace(1).
				WithRequestDump(r).
				Finish()
			errors.Handle(er, w, r)
			return
		}
		//Updates Role
		//c.Project.UserMembers[tid].UpdateRole(project.RoleName(role))
		c.Project.UpdateMember(m, project.RoleName(role))
		err = ps.Add(c.Project)
		if err != nil {
			errors.Handle(err, w, r)
		}

	}
}
