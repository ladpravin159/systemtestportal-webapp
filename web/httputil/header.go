/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package httputil

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/slugs"
)

// The parameter keys of the header
const (
	NewName      = "newName"
	Dashboard    = "dashboard"
	NewAccount   = "newAccount"
	SignedInUser = "signedInUser"
)

//SetHeaderValue is a helper function for setting a value in the header. The value will be URL-Encoded.
func SetHeaderValue(w http.ResponseWriter, key, value string) {
	valueCoded := slugs.URLEncode(value)
	w.Header().Set(key, valueCoded)
}
