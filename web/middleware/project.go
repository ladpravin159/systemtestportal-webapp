/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package middleware

import (
	"net/http"

	"fmt"

	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/group"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

// ProjectStore provides an interface for retrieving projects
type ProjectStore interface {

	// Get looks up the project with the given id. It returns a pointer to the project and true
	// if a matching project was found or nil and false otherwise
	Get(projectID id.ProjectID) (*project.Project, bool, error)
}

// ProjectKey is used to retrieve a project from a request context.
const ProjectKey = "project"

const (
	nonExistentProjectTitle = "The project you requested doesn't exist."
	nonExistentProject      = "It seems that you requested a project that doesn't exist anymore."
)

// Project returns a middleware that retrieves a project from
// a request and injects it into the request context.
// It requires the 'project' parameter containing the id of the
// project and the container middleware.
func Project(store ProjectStore) negroni.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		pName, err := getParam(r, ProjectKey)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		cID, err := getContainerID(r.Context().Value(ContainerKey), r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		p, err := getProject(store, id.NewProjectID(cID, pName), r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		AddToContext(r, ProjectKey, p)
		next(w, r)
	}
}

func getContainerID(v interface{}, r *http.Request) (id.ActorID, error) {
	switch container := v.(type) {
	case *group.Group:
		return container.ID(), nil
	case *user.User:
		return container.ID(), nil
	default:
		return "", internalError(
			fmt.Sprintf("Given container is neither a group nor a user, but <%+v>", v),
			nil,
			r,
		)
	}
}

func getProject(store ProjectStore, projectID id.ProjectID, r *http.Request) (*project.Project, error) {
	p, ok, err := store.Get(projectID)
	if !ok {
		return nil, errors.ConstructStd(http.StatusNotFound, nonExistentProjectTitle, nonExistentProject, r).
			WithLogf("Client request non-existent project %v.", projectID).
			WithStackTrace(1).
			Finish()
	} else if err != nil {
		return nil, internalError(
			fmt.Sprintf("Unable to load project with id: %v.", projectID),
			err,
			r,
		)
	}
	return p, nil
}
