{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Test Sequence List" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="2">
                            <ul>
                                <li>{{T "Choose test sequences to display them in detail." .}}</li>
                            {{ if .Member }}
                                <li>{{T "Add a new test sequence (see button below)." .}}</li>{{ end }}
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>{{T "Buttons" .}}</h5></td>
                    </tr>
                    <tr>
                        <td>
                            <button class="order-3 btn btn-primary align-middle">
                                + <span class="d-none d-sm-inline">{{T "New Test Sequence" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Add a new test sequence." .}}</td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>
<div class="tab-card card" id="tabTestSequences">
    <nav class="navbar navbar-light action-bar p-3 d-print-none">
        <label class="sr-only">Search</label>
        <div class="input-group flex-nowrap">
            <span>
                <input id="test-sequence-query-search" style="width: 100%;" type="search" name="s" class="order-1 search-field form-control float-left"
                       placeholder="Search...">
            </span>
        {{ if .Member }}
            <button class="order-3 btn btn-primary align-middle ml-4 float-right" id="buttonNewTestSequence">
                + <span class="d-none d-sm-inline">New Test Sequence</span>
            </button>
            {{ else }}
            <button class="order-3 btn btn-primary align-middle ml-4 float-right disabled cursor-not-allowed" type="button"
                    id="buttonNewTestSequenceDisabled" data-toggle="tooltip" data-placement="bottom"
                    title="You have to be a member of the project to create a new test sequence">
                + <span class="d-none d-sm-inline">New Test Sequence</span>
            </button>
            {{ end }}

        </div>
    </nav>
    <div class="row tab-side-bar-row">
        <div class="col-md-9 pt-0 pb-3 pl-3 pr-3">

            <table id="table-test-sequences" class="table w-100 d-none">

                <thead>
                    <tr><th style="border-top: none;">
                        <h4>

                            Test Sequences

                            {{ if .Filtered }}
                                with label

                                {{ range .Filtered }}
                                    <span class="badge badge-primary" data-toggle="tooltip" title="{{ .Description }}" id="{{ .Name }}" style="background-color: #{{ .Color }};">
                                        {{ .Name }}
                                        <strong>
                                            <span class="closeX filterBadge clickIcon" id="{{ .Name }}" aria-hidden="true">&times;</span>
                                        </strong>
                                    </span>
                                {{ end }}
                            {{ else }}
                                (All)
                            {{ end }}

                        </h4>
                    </th></tr>
                </thead>

                <tbody>
                    {{range .TestSequences }}
                        <tr class="testSequenceLine"><td id="{{ .Name }}">

                            {{ .Name }}

                            {{ range .Labels }}
                                <span class="badge badge-primary filterBadge float-right" data-toggle="tooltip" title="{{ .Description }}" id="{{ .Name }}" style="background-color: #{{ .Color }};">{{ .Name }}</span>
                            {{ end }}

                        </td></tr>
                    {{ else }}
                        <tr><td>

                            <p class="text-center empty-project-list">
                                <span class="text-muted text-center">
                                    There are no test sequences yet. {{ if .Member }} Do you want to create a new test sequence? {{ end }}
                                </span>
                                <br/><br/>

                                {{ if .Member }}
                                    <button class="btn btn-primary align-middle"
                                            id="buttonFirstTestSequence">+ New Test Sequence</button>
                                {{ end }}
                            </p>
                        </td></tr>
                    {{ end }}
                </tbody>

            </table>

        </div>
        <div class="col-md-3 p-3 tab-side-bar">
            <h4>
                Labels
            {{ if .Member }}
                <button type="button" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#modal-manage-labels" >
                    <i class="fa fa-wrench" aria-hidden="true"></i>
                </button>
                {{ end }}
            </h4>
            <p id="labelContainer">
            {{ with .Project }}
            {{ range .Labels }}
                <span class="badge badge-primary filterBadge clickIcon" data-toggle="tooltip" title="{{ .Description }}" id="{{ .Name }}" style="background-color: #{{ .Color }};">{{ .Name }}</span>
            {{ else }}
                <span class="text-muted">No Labels</span>
            {{ end }}
            {{ end }}
            </p>
        </div>
    </div>
    {{template "modal-manage-labels" . }}
</div>

<!-- Import Scripts here -->
<script src="/static/js/project/testsequences.js"></script>
<script src="/static/js/util/common.js"></script>
<script src="/static/js/project/labels.js"></script>

<script>
    assignButtonsTestSequence();
    $("#printerIcon").removeClass("d-none");
    $("#helpIcon").removeClass("d-none");

    function printer() {
        window.open(currentURL().appendSegment('print').toString(),
                'newwindow',
                'width=600,height=800');
        return false;
    }

    $(() => {

        const table = $('#table-test-sequences');
        const dataTable = table.DataTable({
            "sDom": '<"top">rt<"bottom"p><"clear">',

            "pageLength": 15,
            drawCallback: function() {
                const pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
                pagination.toggle(this.api().page.info().pages > 1);
            },

            "language": {
                "zeroRecords":     "No Test Sequences found matching query ..."
            }
        });

        $("#test-sequence-query-search").on("keyup", function () {
            dataTable.search( this.value ).draw()
        });

        table.removeClass("d-none");
    })

</script>
{{end}}