/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package config

import (
	"bytes"
	"log"
	"os"
	"os/exec"
	"path"
	"testing"
)

// isGuest is used to distinguish between the host test (the test starting the sub test)
// and the guest test which is the actual test executed that might exit during testing.
const isGuest = "EXECUTE_TEST"

func TestBasePath(t *testing.T) {
	if err := checkBasePath(BasePath()); err != nil {
		t.Fatalf("Expected basepath to be correct during testing.\n"+
			"But check returned error: %s", err)
	}
}

func TestLoad(t *testing.T) {
	if os.Getenv(isGuest) == "1" {
		// We are the guest execute the test normally
		executeLoadTest(t)
		return
	}
	// We are the host run the test isolated as different process
	cmd := exec.Command(os.Args[0], "-test.run=TestLoad")
	cmd.Env = append(os.Environ(), isGuest+"=1")
	o := bytes.NewBufferString("")
	// Capture output from the test.
	cmd.Stdout = o

	err := cmd.Run()
	if e, ok := err.(*exec.ExitError); ok && !e.Success() {
		return
	}
	t.Errorf("Expected process to exit since base path is invalid.\nBut did not.")
}

func executeLoadTest(t *testing.T) {
	b := bytes.NewBufferString("")
	log.SetOutput(b)
	defer checkResult(t, b)

	cfgPath := path.Join(BasePath(), "config.ini")
	os.Args = []string{
		"stp",
		"-config-path=" + cfgPath,
		"-debug",
		"-port=80",
		"-host=localhost",
	}
	// Exits due to invalid basepath
	Load()
}

func checkResult(t *testing.T, b *bytes.Buffer) {
	if !Get().Debug {
		t.Errorf("Expected debug to be set to: %t\n"+
			"But got: %t", true, Get().Debug)
	}
	if Get().Port != 80 {
		t.Errorf("Expected port to be set to: %d\n"+
			"But got: %d", 80, Get().Port)
	}
	if Get().Host != "localhost" {
		t.Errorf("Expected host to be set to: %s\n"+
			"But got: %s", "localhost", Get().Host)
	}
	if BasePath() != defaultBasePath {
		t.Errorf("Expected basepath to be '%s'(default) after loading.\n"+
			"But was: '%s'", defaultBasePath, BasePath())
	}
	if t.Failed() {
		t.Logf("Log output was:\n%s", b.String())
	}
}
