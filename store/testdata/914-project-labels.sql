-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
INSERT INTO project_labels (id, project_id, name, description)
VALUES 
    (1, 2, 'Improvement needed', 'This test has a low quality and should be improved'),
    (2, 2, 'Deprecated', 'This test is no longer applicable to new versions and only kept around for historic reasons'),
    (3, 2, 'Short test', 
        'This test, including setup and tear down of the test environment, takes no longer than 10 minutes'),
    (4, 2, 'Long test', 
        'This test, including setup and tear down of the test environment, takes longer than 120 minutes'),
    (5, 1, 'Example label', 'First label for demonstration purposes'),
    (6, 1, 'Unused label', 'This label is not assigned anywhere');

-- +migrate Down
DELETE FROM project_labels;