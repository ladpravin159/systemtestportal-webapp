-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
INSERT INTO test_steps (test_case_version_id, step_index, action, expected_result)
VALUES
    (1, 1, 'Open the application', 'The application should display the start screen'),
    (2, 1, 'Click on the "Execute Test Suite" button', 'The application should display the execution screen'),
    (2, 2, 'Select the "Simple Test Suite"', 'The test case list contains 4 test cases'),
    (2, 3, 'Click on start execution', 'The application shows a progress screen'),
    (2, 4, 'Wait for the execution to finish', 'The execution should complete successfully'),
    (3, 1, 'Click on the "Execute Test Suite" button', 'The application should display the execution screen'),
    (3, 2, 'Select the "Simple Test Suite"', 'The test case list contains 4 test cases'),
    (3, 3, 'Click on start execution', 'The application shows a progress screen'),
    (3, 4, 'Wait for the execution to finish', 'The execution should complete successfully in less than 20 minutes'),
    (4, 1, 'Click on the "Execute Test Suite" button', 'The application should display the execution screen'),
    (
        4, 2, 'Select the "Complex Test Suite"', 
            '- The test case list contains 25 test cases
- The test rule list contains 3 rules'
    ),
    (4, 3, 'Change the value of the "Iterations" rule to 15', 'The label displays the new value'),
    (4, 4, 'Click on start execution', 'The application shows a progress screen'),
    (4, 5, 'Wait for the execution to finish', 'The execution should complete successfully in less than 90 minutes');

-- +migrate Down
DELETE FROM test_steps;