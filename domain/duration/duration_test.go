/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package duration

import (
	"testing"
	"time"
)

const years = 1
const months = 9
const weeks = 2
const days = 3
const hours = 10
const minutes = 40
const seconds = 30
const cumulativeNanoseconds = years*Year +
	months*Month +
	weeks*Week +
	days*Day +
	hours*time.Hour +
	minutes*time.Minute +
	seconds*time.Second

func TestNewDuration(t *testing.T) {
	d := NewDuration(hours, minutes, seconds)
	ms := time.Hour*hours + time.Minute*minutes + time.Second*seconds

	if d.Duration != ms {
		t.Errorf("Duration wasn't saved correctly. Expected %d but was %d", ms, d.Nanoseconds())
	}
}

func TestDuration_ConvertToHHMMSS(t *testing.T) {
	ms := days*Day + time.Hour*hours + time.Minute*minutes + time.Second*seconds
	d := Duration{ms}
	hh, mm, ss := d.ConvertToHHMMSS()
	if hh != hours+days*24 {
		t.Errorf("Hours were calculated incorrect. Expected %d but was %d", hours+days*24, hh)
	}
	if mm != minutes {
		t.Errorf("Minutes were calculated incorrect. Expected %d but was %d", minutes, mm)
	}
	if ss != seconds {
		t.Errorf("Seconds were calculated incorrect. Expected %d but was %d", seconds, ss)
	}
}

func TestDuration_Add_Sub(t *testing.T) {
	duration1 := NewDuration(4, 4, 4)
	duration2 := NewDuration(1, 2, 3)
	sum := NewDuration(5, 6, 7)
	difference := NewDuration(3, 2, 1)

	result := duration1.Add(duration2)
	if result != sum {
		t.Errorf("Addition wasn't calculated correctly. Expected %v+ but was %v+",
			sum, result)
	}
	result = duration1.Sub(duration2)
	if result != difference {
		t.Errorf("Subtraction wasn't calculated correctly. Expected %v+ but was %v+",
			difference, result)
	}
}

func TestDuration_Years(t *testing.T) {
	d := Duration{cumulativeNanoseconds}
	y := d.Years()
	if y != float64(cumulativeNanoseconds)/float64(Year) {
		t.Errorf("Years were calculated incorrect. Expected %f but was %f", float64(cumulativeNanoseconds)/float64(Year), y)
	}
}
func TestDuration_Months(t *testing.T) {
	d := Duration{cumulativeNanoseconds}
	m := d.Months()
	if m != float64(cumulativeNanoseconds)/float64(Month) {
		t.Errorf("Months were calculated incorrect. Expected %f but was %f", float64(cumulativeNanoseconds)/float64(Month), m)
	}
}
func TestDuration_Weeks(t *testing.T) {
	d := Duration{cumulativeNanoseconds}
	w := d.Weeks()
	if w != float64(cumulativeNanoseconds)/float64(Week) {
		t.Errorf("Weeks were calculated incorrect. Expected %f but was %f", float64(cumulativeNanoseconds)/float64(Week), w)
	}
}
func TestDuration_Days(t *testing.T) {
	d := Duration{cumulativeNanoseconds}
	days := d.Days()
	if days != float64(cumulativeNanoseconds)/float64(Day) {
		t.Errorf("Days were calculated incorrect. Expected %f but was %f", float64(cumulativeNanoseconds)/float64(Day), days)
	}
}

func TestDuration_GetMonthInYear(t *testing.T) {
	d := Duration{cumulativeNanoseconds}
	mm := d.GetMonthInYear()
	if mm != months {
		t.Errorf("Months were calculated incorrect. Expected %d but was %d", months, mm)
	}
}
func TestDuration_GetWeekInYear(t *testing.T) {
	d := Duration{cumulativeNanoseconds}
	ww := d.GetWeekInYear()
	if ww != (weeks*7+months*30+days)/7 {
		t.Errorf("Weeks were calculated incorrect. Expected %d but was %d", (weeks*7+months*30+days)/7, ww)
	}
}
func TestDuration_GetDayInYear(t *testing.T) {
	d := Duration{cumulativeNanoseconds}
	dd := d.GetDayInYear()
	if dd != days+weeks*7+months*30 {
		t.Errorf("Days were calculated incorrect. Expected %d but was %d", days+weeks*7+months*30, dd)
	}
}
func TestDuration_GetDayInMonth(t *testing.T) {
	d := Duration{cumulativeNanoseconds}
	dd := d.GetDayInMonth()
	if dd != days+weeks*7 {
		t.Errorf("Days were calculated incorrect. Expected %d but was %d", days+weeks*7, dd)
	}
}
func TestDuration_GetDayInWeek(t *testing.T) {
	d := Duration{cumulativeNanoseconds}
	dd := d.GetDayInWeek()
	if dd != days {
		t.Errorf("Days were calculated incorrect. Expected %d but was %d", days, dd)
	}
}
func TestDuration_GetHourInDay(t *testing.T) {
	d := Duration{cumulativeNanoseconds}
	hh := d.GetHourInDay()
	if hh != hours {
		t.Errorf("Hours were calculated incorrect. Expected %d but was %d", hours, hh)
	}
}
func TestDuration_GetMinuteInHour(t *testing.T) {
	d := Duration{cumulativeNanoseconds}
	mm := d.GetMinuteInHour()
	if mm != minutes {
		t.Errorf("Minutes were calculated incorrect. Expected %d but was %d", minutes, mm)
	}
}
func TestDuration_GetSecondInMinute(t *testing.T) {
	d := Duration{cumulativeNanoseconds}
	ss := d.GetSecondInMinute()
	if ss != seconds {
		t.Errorf("Seconds were calculated incorrect. Expected %d but was %d", seconds, ss)
	}
}
